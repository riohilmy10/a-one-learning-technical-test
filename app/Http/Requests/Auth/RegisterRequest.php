<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // I'm using simple basic validation only
        return [
            'name' => 'required|between:2,50',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'profile_photo' => 'required|image|max:2000|mimes:jpeg,jpg,png'
        ];
    }
}
