<?php

namespace App\Http\Requests\Comments;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Currently the validation rules are the same as create request,
        // but I separate the file as best practice since in real life scenario,
        // most of the time each request has different validation rules
        return [
            'body' => 'required|min:2',
        ];
    }
}
