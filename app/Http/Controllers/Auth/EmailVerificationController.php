<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;

class EmailVerificationController extends Controller
{
    public function verify(Request $request)
    {
        // Get user with matching token
        $user = User::select('id', 'email_verified_at')
            ->where('email_verification_token', $request->token)
            ->first();

        // If no user found, or user email already verified, return Link Expired page
        if (!$user || $user->email_verified_at) {
            return view('email-verification-error');
        }

        // Update user email verified at column
        $user->update(['email_verified_at' => Carbon::now()]);

        return view('email-verified');
    }
}
