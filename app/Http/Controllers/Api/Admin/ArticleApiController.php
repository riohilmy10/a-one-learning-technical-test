<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Http\Requests\Articles\CreateRequest;
use App\Http\Requests\Articles\UpdateRequest;

class ArticleApiController extends ApiController
{
    /**
     * API to get list of Articles
     */
    public function index(Request $request)
    {
        // I'm using laravel default pagination here, I think it's a best practice for list API as most of the time
        // listing requires pagination
        $article_list = Article::withCount('comments') // Get number of comments
            ->orderBy('updated_at', 'desc') // Sort articles by latest updated_at first
            ->paginate(10); // 10 items per page

        return $this->formatResourceResponse(
            $article_list,
            200,
            trans('message.get_list_success', ['name' => 'articles'])
        );
    }
    
    /**
     * API to get details of an Article
     */
    public function details($article_id)
    {
        $article = Article::whereId($article_id)
            ->with(
                [
                    'comments' => function ($query) { // Get Eloquent has many relationship with comments
                        $query->orderBy('updated_at', 'desc'); // Sort comments by latest updated_at first
                    }, 'comments.user' => function ($query) { // Get Eloquent belongs to relationship with user through comments
                        $query->select('id', 'name'); // Select only id and commenter name
                    }
                ]
            )
            ->first();
        
        // Return error if no article found by given article_id
        if (!$article) {
            return $this->formatErrorResponse(
                trans('message.not_found', ['name' => 'article']),
                404
            );
        }
        
        return $this->formatResourceResponse(
            $article,
            200,
            trans('message.get_details_success', ['name' => 'article'])
        );
    }
    
    /**
     * API to create an Article
     */
    public function create(CreateRequest $request)
    {
        Article::create(
            [
                'title' => $request->title,
                'description' => $request->description
            ]
        );

        return $this->formatSuccessResponse(
            trans('message.create_success', ['name' => 'article']),
            200,
        );
    }
    
    /**
     * API to update an Article
     */
    public function update(UpdateRequest $request, $article_id)
    {
        $article = Article::find($article_id);

        // Return error if no article found by given article_id
        if (!$article) {
            return $this->formatErrorResponse(
                trans('message.not_found', ['name' => 'article']),
                404
            );
        }

        $article->update(
            [
                'title' => $request->title,
                'description' => $request->description
            ]
        );

        return $this->formatSuccessResponse(
            trans('message.update_success', ['name' => 'article']),
            200,
        );
    }
    
    /**
     * API to delete an Article
     */
    public function delete($article_id)
    {
        $article = Article::find($article_id);

        // Return error if no article found by given article_id
        if (!$article) {
            return $this->formatErrorResponse(
                trans('message.not_found', ['name' => 'article']),
                404
            );
        }

        $article->delete();

        return $this->formatSuccessResponse(
            trans('message.delete_success', ['name' => 'article']),
            200,
        );
    }
}
