<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\User;

class UserApiController extends ApiController
{
    /**
     * API to get list of Users
     */
    public function index(Request $request)
    {
        // I'm using laravel default pagination here, I think it's a best practice for list API as most of the time
        // listing requires pagination
        $user_list = User::where('email', '!=', env('ADMIN_EMAIL', 'admin@myaone.my'))
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        // Return my custom resource success response
        return $this->formatResourceResponse(
            $user_list,
            200,
            trans('message.get_list_success', ['name' => 'users'])
        );
    }
}
