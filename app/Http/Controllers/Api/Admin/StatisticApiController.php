<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Comment;
use App\Models\User;

class StatisticApiController extends ApiController
{
    /**
     * API to get statistics
     */
    public function index(Request $request)
    {
        // Requirement said number of comments per user, but I believe it's supposed to be average number of comments per user
        // Since if literally want statistic number of comments per user, then I have to list down all users
        // with each user has number of comments which for me it doesn't make sense for statistic
        // but I'll just do both average count and number of comments per user
        $comments_unique_user_count = Comment::distinct()->count('user_id'); // Get comments with unique user_id as divider to get average count
        $comments_count = Comment::count(); // Get all comments count as divisor to get average count
        $comments_per_user_average = $comments_count / $comments_unique_user_count; // Get average comments per user count

        $return_data = [
            'articles_count' => Article::count(),
            'comments_count' => $comments_count,
            'users_count' => User::count(),
            'comments_per_user_average' => $comments_per_user_average,
            'comments_per_user_count' => User::select('id','name')->withCount('comments')->get()
        ];

        // Return my custom resource success response
        return $this->formatResourceResponse(
            $return_data,
            200,
            trans('message.get_statistic_success')
        );
    }
}
