<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;

class LoginApiController extends ApiController
{
    /**
     * API to handle an authentication attempt.
     */
    public function login(LoginRequest $request)
    {
        try {
            $credentials = $request->only('email', 'password');
            $credentials['email'] = strtolower($credentials['email']);

            // If correct credentials
            if (auth()->attempt($credentials)) {
                $user = auth()->user();
                
                // Return error if user email is not verified yet
                if (!$user->email_verified_at) {
                    // Return my custom error response
                    return $this->formatErrorResponse(
                        trans('auth.email_not_verified'),
                        403
                    );
                }

                // I'm using Passport token scope as role because the requirements only ask for APIs differentiated by Admin and Public User
                // So instead of using Spatie Laravel Permission role, I think Passport token scope is enough
                // if logged in user email is admin email stated in env, token scope is 'admin', else is 'user'
                $user->email == env('ADMIN_EMAIL', 'admin@myaone.my') ? $scope = 'admin' : $scope = 'user';
                // Create passport access token
                $access_token = $user->createToken('Passport API token', [$scope])->accessToken;
                // Return access token as response in order to call other APIs that require authorization
                $return_data = [
                    'access_token' => $access_token
                ];

                // Return my custom resource success response
                return $this->formatResourceResponse(
                    $return_data,
                    200,
                    trans('auth.login_success')
                );
            } else { // Return error if invalid credentials
                return $this->formatErrorResponse(
                    trans('auth.invalid_credentials'),
                    401
                );
            }
        } catch (\Exception $e) {
            // Log down any errors occured during script execution
            \Log::error($e);

            // Return my custom error response
            return $this->formatErrorResponse(
                trans('message.internal_server_error'),
                500
            );
        }
    }
}
