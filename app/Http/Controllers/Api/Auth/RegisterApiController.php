<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Hash;
use App\Traits\EmailTrait;
use App\Http\Requests\Auth\RegisterRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class RegisterApiController extends ApiController
{
    use EmailTrait;

    /**
     * API to create a new user instance.
     */
    protected function register(RegisterRequest $request)
    {
        try {
            // Convert email to all lowercase
            $email = strtolower($request->email);

            // Upload profile photo into profile_photos directory in storage, with random filename by default
            // Run php artisan storage:link in order to link public storage with local storage
            $profile_photo_filename = Storage::disk('public')->putFile('profile_photos', $request->profile_photo);

            // Create a random verification token
            $verification_token = $this->generateToken();

            // I'm using transaction() to rollback the record in case error occurs in sending email
            // like forgot to put sendgrid credentials in .env file
            DB::transaction(function () use ($request, $email, $verification_token, $profile_photo_filename) {
                // Create user record
                User::create(
                    [
                        'name' => $request->name,
                        'email' => $email,
                        'email_verification_token' => $verification_token,
                        'password' => Hash::make($request->password),
                        'profile_photo' => $profile_photo_filename // Store the filepath and filename
                    ]
                );

                // Send email verification
                $this->sendEmailVerification($email, $request->name, $verification_token);
            });

            // Return my custom success response
            return $this->formatSuccessResponse(
                trans('auth.register_success'),
                200
            );
        } catch (\Exception $e) {
            \Log::error($e);
            
            // Return my custom error response
            return $this->formatErrorResponse(
                trans('message.internal_server_error'),
                500
            );
        }
    }
}
