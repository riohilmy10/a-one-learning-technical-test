<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Comment;
use App\Http\Requests\Comments\CreateRequest;
use App\Http\Requests\Comments\UpdateRequest;

class CommentApiController extends ApiController
{
    /**
     * API to create a Comment
     */
    public function create(CreateRequest $request, $article_id)
    {
        $article = Article::find($article_id);

        // Return error if no article found by given article_id
        if (!$article) {
            return $this->formatErrorResponse(
                trans('message.not_found', ['name' => 'article']),
                404
            );
        }

        // Create comment through eloquent relationship from article
        $article->comments()->create(
            [
                'body' => $request->body,
                'user_id' => auth()->user()->id
            ]
        );

        return $this->formatSuccessResponse(
            trans('message.create_success', ['name' => 'comment']),
            200,
        );
    }
    
    /**
     * API to update a Comment
     */
    public function update(UpdateRequest $request, $article_id, $comment_id)
    {
        $article = Article::find($article_id);

        // Return error if no article found by given article_id
        if (!$article) {
            return $this->formatErrorResponse(
                trans('message.not_found', ['name' => 'article']),
                404
            );
        }

        // Get comment with given comment_id through eloquent relationship from article
        $comment = $article->comments()
            ->where('id', $comment_id)
            ->first();

        // Return error if no comment found in the article by given comment_id
        if (!$comment) {
            return $this->formatErrorResponse(
                trans('message.not_found', ['name' => 'comment']),
                404
            );
        } else if ($comment->user_id != auth()->user()->id) { // else if current logged in user_id is not the comment owner, return error
            return $this->formatErrorResponse(
                trans('message.comment_update_not_owner_failed'),
                403
            );
        }
        
        $comment->update(
            [
                'body' => $request->body,
            ]
        );

        return $this->formatSuccessResponse(
            trans('message.update_success', ['name' => 'comment']),
            200,
        );
    }
    
    /**
     * API to delete a Comment
     */
    public function delete($article_id, $comment_id)
    {
        $article = Article::find($article_id);

        // Return error if no article found by given article_id
        if (!$article) {
            return $this->formatErrorResponse(
                trans('message.not_found', ['name' => 'article']),
                404
            );
        }

        // Get comment with given comment_id through eloquent relationship from article
        $comment = $article->comments()
            ->where('id', $comment_id)
            ->first();

        // Return error if no comment found in the article by given comment_id
        if (!$comment) {
            return $this->formatErrorResponse(
                trans('message.not_found', ['name' => 'comment']),
                404
            );
        } else if ($comment->user_id != auth()->user()->id) { // else if current logged in user_id is not the comment owner, return error
            return $this->formatErrorResponse(
                trans('message.comment_delete_not_owner_failed'),
                403
            );
        }

        $comment->delete();

        return $this->formatSuccessResponse(
            trans('message.delete_success', ['name' => 'comment']),
            200,
        );
    }
}
