<?php

namespace App\Traits;

use Illuminate\Support\Str;
use App\Services\SendgridService;

/**
* Traits for sending email
*/
trait EmailTrait
{
    
    /**
    * Method to generate random hashed token
    */
    public function generateToken()
    {
        return hash('sha256', Str::random(40));
    }

    /**
    * Method to send email verification to sendgrid send email API service
    */
    public function sendEmailVerification($email, $name, $token)
    {
        $sendgrid_service = new SendgridService();
        $subject = 'A One Learning Email verification';

        // Substitutions for dynamic text in the email
        $substitutions = [
            'name' => $name,
            'link' => route('email-verification', $token)
        ];

        $from = [
            'email' => config('mail.from.address'),
            'name' => config('mail.from.name')
        ];

        // Sendgrid email template id (Configured in Sendgrid dashboard)
        $template_id = config('mail.sendgrid.template.email_verification');

        return $sendgrid_service->send($from, $email, $subject, $substitutions, $template_id);
    }
}
