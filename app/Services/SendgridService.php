<?php

namespace App\Services;

use GuzzleHttp\Client;

/**
* Service for Sendgrid Email
*/
class SendgridService
{
    /**
    * Method to send email through third party Sendgrid service
    */
    public function send($from, $to, $subject, $substitutions, $template_id)
    {
        try {
            $client = new Client();
            $recipient = [];

            // If sending email to more than 1 person
            if (is_array($to)) {
                foreach ($to as $email_to) {
                    $recipient[]['email'] = $email_to;
                }
            } else {
                $recipient[]['email'] = $to;
            }

            // Data to be sent to sendgrid API
            $data = [
                'personalizations' => [
                    [
                        'to' => $recipient,
                        'dynamic_template_data' => $substitutions
                    ],
                ],
                'from' => [
                    'email' => $from['email'],
                    'name' => $from['name']
                ],
                'subject' => $subject,
                'template_id' => $template_id
            ];

            // Call Sendgrid send email API
            $response = $client->post(
                config('mail.sendgrid.send_url'), // Sendgrid send email API URL
                [
                    'headers' => [
                        'Authorization' => 'Bearer '. config('mail.sendgrid.api_key'), // Sendgrid API token
                    ],
                    'json' => $data
                ]
            );

            // return true (success) only if sendgrid response is 202 or 200 as per documentation
            if ($response->getStatusCode() === 202 || $response->getStatusCode() === 200) {
                return true;
            }

            // return false (failed) if sendgrid response is other than 202 or 200
            return false;
        } catch (\Exception $e) {
            // Log down any error
            \Log::error($e);

            // Throw back the exception to previous function
            throw new \Exception($e);
        }
    }
}
