<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'invalid_credentials' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'register_success' => 'We have sent you a verification email, please verify your email to login.',
    'login_success' => 'Login attempt successful.',
    'logout_success' => 'Logout successful.',
    'email_not_verified' => 'Unable to login, please verify your email first.',
];
