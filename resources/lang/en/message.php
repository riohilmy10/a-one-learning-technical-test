<?php

return [
    'internal_server_error' => 'Something went wrong. Please contact our customer service or admin.',
    'get_list_success' => 'Successfuly fetched list of :name.',
    'get_details_success' => 'Successfuly fetched details of :name.',
    'create_success' => ':Name has been successfuly created.',
    'update_success' => ':Name has been successfuly updated.',
    'delete_success' => ':Name has been successfuly deleted.',
    'not_found' => ':Name not found.',
    'comment_update_not_owner_failed' => 'Cannot update other people\'s comment.',
    'comment_delete_not_owner_failed' => 'Cannot delete other people\'s comment.',
    'get_statistic_success' => 'Successfuly fetched statistics',
];
