<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::where('email', env('ADMIN_EMAIL', 'admin@myaone.my'))->first();

        if (!$admin) {
            User::create(
                [
                    'name' => 'My A One Admin',
                    'email' => env('ADMIN_EMAIL', 'admin@myaone.my'),
                    'password' => Hash::make(env('ADMIN_PASSWORD', 'admin123')),
                    'email_verified_at' => Carbon::now(),
                    'email_verification_token' => hash('sha256', Str::random(40))
                ]
            );
        }
    }
}
