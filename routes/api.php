<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(
    ['namespace' => 'Api', 'as' => 'api.'],
    function () {
        // Guest API
        Route::post('login', 'Auth\LoginApiController@login')->name('login');
        Route::post('register', 'Auth\RegisterApiController@register')->name('register');
        
        // Authenticated only APIs
        Route::group(
            ['middleware' => 'auth:api'],
            function () {
                // Admin APIs
                Route::group(
                    ['prefix' => 'admin', 'middleware' => 'scope:admin', 'namespace' => 'Admin', 'as' => 'admin.'],
                    function () {
                        Route::get('/users', 'UserApiController@index')->name('users.list');
                        Route::get('/statistics', 'StatisticApiController@index')->name('statistics');

                        // Admin Articles API
                        Route::group(
                            ['prefix' => 'articles', 'as' => 'articles.'],
                            function () {
                                Route::get('/', 'ArticleApiController@index')->name('list');
                                Route::get('/{article_id}', 'ArticleApiController@details')->name('details');
                                Route::post('/', 'ArticleApiController@create')->name('create');
                                Route::put('/{article_id}', 'ArticleApiController@update')->name('update');
                                Route::delete('/{article_id}', 'ArticleApiController@delete')->name('delete');
                            }
                        );
                    }
                );
                
                // User APIs
                Route::group(
                    ['middleware' => 'scope:user', 'namespace' => 'User'],
                    function () {
                        
                        // User Comments API
                        Route::group(
                            ['prefix' => 'comments', 'as' => 'comments.'],
                            function () {
                                Route::post('/{article_id}', 'CommentApiController@create')->name('create');
                                Route::put('/{article_id}/{comment_id}', 'CommentApiController@update')->name('update');
                                Route::delete('/{article_id}/{comment_id}', 'CommentApiController@delete')->name('delete');
                            }
                        );
                    }
                );
            }
        );

        // Unauthenticated User APIs
        Route::group(
            ['namespace' => 'User'],
            function () {
                // User Articles API
                Route::group(
                    ['prefix' => 'articles', 'as' => 'articles.'],
                    function () {
                        Route::get('/', 'ArticleApiController@index')->name('list');
                        Route::get('/{article_id}', 'ArticleApiController@details')->name('details');
                    }
                );
            }
        );
    }
);
